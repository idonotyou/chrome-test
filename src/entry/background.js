chrome.action.onClicked.addListener((tab) => {
  if (!tab.url.includes("chrome://")) {
    // 打卡侧边栏面板
    chrome.sidePanel
      .setPanelBehavior({ openPanelOnActionClick: true })
      .catch((error) => console.error(error));

    // 执行脚本函数
    chrome.scripting.executeScript({
      target: { tabId: tab.id },
      function: reddenPage,
    });
  }
});

function reddenPage() {}
